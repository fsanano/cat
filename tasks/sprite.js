'use strict';
const $ 			= require('gulp-load-plugins')();
const gulp 			= require('gulp');
const spritesmith	= require('gulp.spritesmith')
const optionsSprite = {
	src: 'dev/img/sprite/*',
	dist: 'build/img',
	stylDist: 'dev/stylus/'
};
gulp.task('sprite', function(){
	var spriteData =
		gulp.src(optionsSprite.src, { since: gulp.lastRun('sprite') }) // путь, откуда берем картинки для спрайта
			.pipe($.plumber())
			.pipe($.spritesmith({
				imgName: 'sprite.png',
				retinaImgName: 'sprite@2x.png',
				cssName: 'sprite.styl',
				cssFormat: 'stylus',
				padding: 10,
				retinaSrcFilter: 'dev/img/sprite/*@2x.png',
				algorithm: 'top-down',
				cssTemplate: 'stylus.template.mustache',
			}));
	spriteData.img.pipe(gulp.dest(optionsSprite.dist));
	spriteData.css.pipe(gulp.dest(optionsSprite.stylDist));
	return spriteData;
})
