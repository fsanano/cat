'use strict';
const $ 			= require('gulp-load-plugins')();
const gulp 			= require('gulp');

const optionsPug 	= {
	src: 'dev/pages/**/*.pug',
	dist: 'build'
}

var locale = {name: 'name'}

gulp.task('pug', function(){
	return gulp.src( optionsPug.src )
		.pipe( $.plumber() )

		.pipe( $.changed( 'build', { extension: '.html' } ) )

		.pipe ( $.pug({
			pretty: true
		}) )

		.pipe( gulp.dest( optionsPug.dist ) );

});