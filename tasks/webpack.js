'use strict';

const gulp = require('gulp');
const named = require('vinyl-named');
const webpackStream = require('webpack-stream');
const webpack = webpackStream.webpack;
const $ = require('gulp-load-plugins')();
const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

gulp.task('webpack', function(callback) {
	let firstBuildReady = false;
	let options = require('../webpack.config.js');
	if (!isDevelopment) {
		options.plugins.push(
			new webpack.optimize.UglifyJsPlugin({
				compress: {
					warnings: true,
					unsafe:   true
				}
			})
		);
	}

	return gulp.src(['dev/common/js/*.js', 'dev/pages/**/*.js'])
		.pipe($.plumber({}))
		.pipe(named())
		.pipe(webpackStream( options ))
		.pipe(gulp.dest('./build'))
		.on('data', function() {
			callback();
		});
});