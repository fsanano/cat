'use strict';

const $ 			= require('gulp-load-plugins')();
const gulp 			= require('gulp');
const optionsImg 	= {
	path: ['dev/common/img/**/*', '!dev/common/img/sprite/*'],
	dist: 'build/img/',
	crop: false
}

gulp.task('img', function(){
	return gulp.src( optionsImg.path , { since: gulp.lastRun('img') } )
		.pipe( $.newer( optionsImg.dist ) )
		// .pipe($.imageResize({
		// 	width : 400,
		// 	// crop : true,
		// 	upscale : false
		// }))
		.pipe( $.image({
			zopflipng: false
		}) )
		.pipe( gulp.dest( optionsImg.dist ) );
});
