'use strict';

import $ from 'jquery';
import 'slick-carousel';
import matchHeight from 'jquery-match-height'
import 'lity';

$(document).ready(function(){
	var aside          = $('.aside');
	var fancy          = $('.aside__fancy');
	var btn            = $('.header__burger_btn');
	var closeAside     = $('.aside__close');

	var modalFancy     = $('.modal__fancy');
	var modalClose     = $('.modal__close');

	var showaside = function(){

		aside.animate({
			left: 0,
			opacity: 1
		}, 500, 'swing');

		fancy.fadeIn();

		btn.addClass('active');

	}

	var hideaside = function(){

		btn.removeClass('active');

		aside.animate({
			left: -aside.width(),
			opacity: 1
		}, 500, 'swing')

		fancy.fadeOut();
	}



	btn.click(function(){
		if( $(this).hasClass('active') ){
			hideaside();
		} else {
			showaside();
		}
	});

	fancy.click(function(){
		hideaside();
	});

	closeAside.click(function(){
		hideaside();
	});


	var showModal = function(modal){
		modalFancy.fadeIn()
		modal.fadeIn()
	}

	var hideModal = function(modal){
		modalFancy.fadeOut()
		modal.fadeOut()
	}

	$('.item__title').click(function(e){
		e.preventDefault();
		showModal($('.modal_buyitem'));
	});

	modalFancy.click(function(e){
		hideModal($('.modal_buyitem'));
	})
	modalClose.click(function(e){
		hideModal($(this).parent());
	})

	$('.item_slide, .item_content .item__content, .footer__menu_item').matchHeight({
		byRow: true
	});

	$('.slider_main').slick({
		dots: false,
		infinity: true,
		slidesToShow: 3,
		arrows: true,
		slidesToScroll: 3,
		responsive: [
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 580,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

})

